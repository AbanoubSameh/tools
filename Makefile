CC := gcc
CFLAGS := -std=c99 -g #-Wall -Wextra -Werror

ODIR := obj
SRC := $(wildcard *.c)
OBJ := $(SRC:%.c=${ODIR}/%.o)
HDR := $(wildcard *.h)

all: $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o out

${ODIR}/%.o : %.c $(HDR)
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f ${ODIR}/*

rebuild: clean all
