// Copyright (c) 2019 Abanoub Sameh

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include "classes.h"

char* strduplic(char* str) {
    char *new_str = malloc(strlen(str) + 1);
    if (new_str == NULL) {
        return NULL;
    }
    strcpy(new_str, str);
    return new_str;
}

// operand
Operand* new_Operand(void *op, uint8_t type) {
    Operand *operand = malloc(sizeof(Operand));
    operand->type = type;
    if (type == CHR) {
        printf("op created at: %ld with value: %s\n", (long) operand, (char*) op);
    operand->op = (void*) strduplic(op);
    } else if (type == VDU || type == VDB) {
        printf("op created at: %ld with value: %ld\n", (long) operand, (long) op);
    operand->op = op;
    }
    return operand;
}

void delete_Operand(Operand *operand) {
    if (operand->type == CHR) {
        free(operand->op);
    } else if (operand->type == VDU) {
        delete_Uni_gate(operand->op);
    } else if (operand->type == VDB) {
        delete_Bin_gate(operand->op);
    } else {
        printf("wrong type, could not delete operand\n");
    }
    free(operand);
}

// Uni_gate
Uni_gate* new_Uni_gate() {
    Uni_gate *gate = malloc(sizeof(Uni_gate));
    gate->operand1 = NULL;
    printf("gate created at: %ld\n", (long) gate);
    return gate;
}

void set_Ugate_type(Uni_gate *gate, uint8_t type) {
    gate->type = type;
    printf("gate at %ld has type: %d\n", gate, type);
}

void set_Ugate_operand1(Uni_gate *gate, Operand *op) {
    gate->operand1 = op;
    printf("gate at %ld has op1: %ld\n", (long) gate, (long) op);
}

void delete_Uni_gate(Uni_gate *gate) {
    delete_Operand(gate->operand1);
    free(gate);
}

// Bin_gate
Bin_gate* new_Bin_gate() {
    Bin_gate *gate = malloc(sizeof(Bin_gate));
    gate->operand1 = NULL;
    gate->operand2 = NULL;
    printf("gate created at: %ld\n", (long) gate);
    return gate;
}

void set_Bgate_type(Bin_gate *gate, uint8_t type) {
    gate->type = type;
    printf("gate at %ld has type: %d\n", gate, type);
}

void set_Bgate_operand1(Bin_gate *gate, Operand *op) {
    gate->operand1 = op;
    printf("gate at %ld has op1: %ld\n", (long) gate, (long) op);
}

void set_Bgate_operand2(Bin_gate *gate, Operand *op) {
    gate->operand2 = op;
    printf("gate at %ld has op2: %ld\n", (long) gate, (long) op);
}

void delete_Bin_gate(Bin_gate *gate) {
    delete_Operand(gate->operand1);
    delete_Operand(gate->operand2);
    free(gate);
}

// Inst
Inst* new_Inst(Operand *current) {
    Inst *inst = malloc(sizeof(Inst));
    inst->current = current;
    inst->next = NULL;
    printf("a new instruction was created at %ld\n", inst);
    return inst;
}

void set_inst_next(Inst *inst, Inst *next) {
    inst->next = next;
    printf("inst %ld set as next for inst %ld\n", next, inst);
}

void delete_Inst(Inst *toDel) {
    delete_Operand(toDel->current);
    free(toDel);
}

