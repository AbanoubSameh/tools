// Copyright (c) 2019 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stack.h"

extern uint8_t stack[];

int push(uint8_t num) {
    if (strlen((char*) stack) < STACKSIZE) {
        stack[strlen((char*) stack)] = num;
        return 1;
    } else {
        printf("stack is full\n");
        return -1;
    }
}

int pop() {
    if (stack_is_empty(stack)) {
        printf("stack is empty\n");
        return -1;
    } else {
        uint8_t val = stack[strlen((char*) stack) - 1];
        stack[strlen((char*) stack) - 1] = 0;
        return val;
    }
}

int peek() {
    if (!stack_is_empty(stack)) {
        return stack[strlen((char*) stack) - 1];
    } else {
        printf("stack is empty\n");
        return -1;
    }
}

bool stack_is_empty(uint8_t *stack) {
    if (strlen((char*) stack) == 0) {
        return true;
    } else {
        return false;
    }
}

void print_stack() {
    printf("stack bottom\n");
    for (int i = 0; i < (int) strlen((char*) stack); i++) {
        printf("%d\n", stack[i]);
    }
    printf("stack top\n");
}

int empty_stack() {
    if (stack_is_empty(stack)) {
        printf("stack is already empty\n");
        return -1;
    } else {
        memset(stack, 0, STACKSIZE);
        return 1;
    }
}


