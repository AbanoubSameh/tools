// Copyright (c) 2019 Abanoub Sameh

#ifndef _queue_h_
#define _queue_h_

#include <stdbool.h>

#ifndef QUEUESIZE
#define QUEUESIZE 257
#endif

int enqueue(char*);
int dequeue(int);
int queue_append(char);
int empty_queue();

bool queue_is_empty();

int queue_len();
int queue_free();

#endif
