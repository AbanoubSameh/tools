// Copyright (c) 2019 Abanoub Sameh

#ifndef _stack_h_
#define _stack_h_

#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>
#include <string.h>

#ifndef STACKSIZE
#define STACKSIZE 257
#endif

int push(uint8_t);
int pop();
int peek();

bool stack_is_empty(uint8_t*);

int empty_stack();

void print_stack();

#endif

