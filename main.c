// Copyright (c) 2019 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lexer.h"
#include "parser.h"
#include "queue.h"
#include "stack.h"
#include "code_gen.h"

char in_file_name[257];
FILE *in_file;
// output file to be added

void io_init() {
    strcpy(in_file_name, "input");
    if (!(in_file = fopen(in_file_name, "r"))) {
        printf("Error openning file\n");
        exit(-1);
    }
    // output file to be openned
}

int main() {

    io_init();

    //uint8_t lexem;
    //while ((lexem = lexer()) != 255) {
        //printf("%d\n", lexem);
    //}

    Inst *AST = parser();

    gen_Inst(AST);

    //printf("queue size: %d\n", QUEUESIZE);
    //printf("stack size: %d\n", STACKSIZE);

    fclose(in_file);

}
