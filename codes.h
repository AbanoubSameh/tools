// Copyright (c) 2019 Abanoub Sameh

#ifndef _codes_h
#define _codes_h

// this file contains definitions for token ids
// to make it easier to write grammer rules for the parser

// ** this file should absolutely be used with care, it can really mess things up **
// maybe I should have used an enum instead

// generic types which are only three and they start from 1
#define STR             1
#define NUM             2
#define DBL             3

// string definitions which begin from 64
#define VOID            64
#define LOOP            65
#define INT             66
#define DOUBLE          67
#define IF              68

// tokens definitions which by the way begin from 128
#define PLS             128
#define MNS             129
#define TMS             130
#define DVD             131
#define MDL             132
#define INC             133
#define DEC             134
#define SPC             135
#define TAB             136
#define NLNE            137
#define ORB             138
#define CRB             139
#define OWB             140
#define CWB             141
#define OSB             142
#define CSB             143
#define OR              144
#define AND             145
#define NOT             146
#define SMCLN           147
#define EQL             148

// non terminals which begin from 192
#define identifier      192
#define variable        193
#define declaration     194
#define assignment      195
#define LOP             196
#define SOP             197
#define UOP             198
#define LOPER           199
#define SOPER           200
#define UOPER           201

// testing the parser with math
#define ADD             202
#define MUL             203
#define POS             204
#define ADDOP           205
#define MULOP           206
#define ASSIGN          207
#define ASSIGNOP        208
#define TYPE            209
#define DECL            210
#define VAR             211
#define UN              212
#define INCOP           213
#define UNOP            214

#endif

