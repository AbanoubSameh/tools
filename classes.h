// Copyright (c) 2019 Abanoub Sameh

#ifndef _classes_h_
#define _classes_h_

#include <inttypes.h>

#define CHR     223
#define VDU     224
#define VDB     225

typedef struct Operand {
    uint8_t type;
    void *op;
} Operand;

typedef struct Uni_gate {
    uint8_t type;
    Operand *operand1;
} Uni_gate;

typedef struct Bin_gate {
    uint8_t type;
    Operand *operand1;
    Operand *operand2;
} Bin_gate;

typedef struct Inst {
    Operand *current;
    struct Inst *next;
} Inst;

// this should be put in a Operand file
Operand* new_Operand(void*, uint8_t);
void delete_Operand(Operand*);

// this should be put in a Uni_gate file
Uni_gate* new_Uni_gate();

void set_Ugate_type(Uni_gate*, uint8_t);
void set_Ugate_operand1(Uni_gate*, Operand*);
void delete_Uni_gate(Uni_gate*);

// this should be put in a Bin_gate file
Bin_gate* new_Bin_gate();

void set_Bgate_type(Bin_gate*, uint8_t);
void set_Bgate_operand1(Bin_gate*, Operand*);
void set_Bgate_operand2(Bin_gate*, Operand*);
void delete_Bin_gate(Bin_gate*);

// this should be put in a Inst file
Inst* new_Inst(Operand*);

void set_inst_next(Inst*, Inst*);
void delete_Inst(Inst*);


#endif

