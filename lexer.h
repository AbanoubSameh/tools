// Copyright (c) 2019 Abanoub Sameh

#ifndef _lexer_h_
#define _lexer_h_

#include <inttypes.h>

uint8_t lexer();

int get_line_count();
int get_char_count();
int get_char_count_in_line();

const char* get_token(int);
char* get_string();

#endif

