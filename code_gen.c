// Copyright (c) 2019 Abanoub Sameh

#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>

#include "lexer.h"

#include "code_gen.h"
#include "codes.h"

void gen_Uni_gate(Uni_gate *root) {
    gen_op(root->operand1);
    printf("%s ", get_token(root->type));
}

void gen_Bin_gate(Bin_gate *root) {
    gen_op(root->operand1);
    gen_op(root->operand2);
    printf("%s ", get_token(root->type));
}

void gen_op(Operand *root) {
    //printf("came here\n");
    if (root->type == CHR) {
        printf("%s ", (char*) root->op);
    } else if (root->type == VDU) {
        //printf("Ugate\n");
        gen_Uni_gate((Uni_gate*) root->op);
    } else if (root->type == VDB) {
        //printf("Bgate\n");
        gen_Bin_gate((Bin_gate*) root->op);
    } else {
        printf("wrong operand type\n");
    }
}

void gen_Inst(Inst *toGen) {
    if (toGen == NULL) {
        return;
    }
    gen_op(toGen->current);
    printf("\n");
    gen_Inst(toGen->next);
    delete_Inst(toGen);
}

