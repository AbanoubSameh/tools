// Copyright (c) 2019 Abanoub Sameh

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "queue.h"

extern char queue[];

int enqueue(char *str) {
    if (queue_free() < (int) strlen(str)) {     // need to take another look on this
        printf("not enough space in the queue\n");
        return -1;
    } else {
        strcat(queue, str);
        return 1;
    }
}

int dequeue(int num) {
    if (queue_len() < num) {
        printf("can\'t dequeue more than %d\n", queue_len());
        strcpy(queue, "");
        return -1;
    } else {
        strcpy(queue, queue + num);
        return 1;
    }
}

bool queue_is_empty() {
    if (queue_len() == 0) {
        return true;
    } else {
        return false;
    }
}

int queue_append(char chr) {
    if (queue_free() == 0) {
        printf("queue is full\n");
        return -1;
    }
    char temp[2];
    temp[0] = chr;
    temp[1] = 0;
    strcat(queue, temp);
    return 1;
}

int empty_queue() {
    if (queue_is_empty()) {
        printf("queue is already empty\n");
        return -1;
    }
    strcpy(queue, "");
    return 1;
}

int queue_len() {
    return strlen(queue);
}

int queue_free() {
    return QUEUESIZE - strlen(queue) - 1;
}
