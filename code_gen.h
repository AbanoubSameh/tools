// Copyright (c) 2019 Abanoub Sameh

#ifndef _code_gen_h_
#define _code_gen_h_

#include "classes.h"

void gen_Inst(Inst*);
void gen_op(Operand*);
void gen_Bin_gate(Bin_gate*);

#endif

