// Copyright (c) 2019 Abanoub Sameh

#ifndef _parser_h_
#define _parser_h_

#include "classes.h"

Operand *PADD();
Operand *PASSIGN();

Inst *parser();

#endif

