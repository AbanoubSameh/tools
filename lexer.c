// Copyright (c) 2019 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "queue.h"
#include "lexer.h"

#ifdef PALLLEXER
#define PLINECOUNT
#define PCHARCOUNT
#define PCHARCOUNTIL
#define PCHAR
#define PMATCHES
#define PQUEUE
#define PTEMP
#define PSTRING
#define PEOF
#define PRETTOKEN
#endif

extern FILE *in_file;
extern char in_file_name[];

// token and text lists
const char *tokens[] = {
    "+",
    "-",
    "*",
    "/",
    "%",
    "++",
    "--",
    " ",
    "\t",
    "\n",
    "(",
    ")",
    "{",
    "}",
    "[",
    "]",
    "||",
    "&&",
    "!",
    ";",
    "=",
    "\0"
};

const char *strings[] = {
    "void",
    "loop",
    "int",
    "double",
    "if",
    "\0"
};

// storage for lexer that will be returned
char queue[QUEUESIZE];
char string[QUEUESIZE];
int line_count = 1;
int char_count = 0;
int char_count_in_line = 0;

int get_line_count() {
    return line_count;
}

int get_char_count() {
    return char_count;
}

int get_char_count_in_line() {
    return char_count_in_line;
}

char* get_string() {
    return string;
}

// returns token when givin a token id
const char* get_token(int num) {
    if (num >= 128) {
        return tokens[num - 128];
    } else if (num >= 64){
        return strings[num - 64];
    } else if (num == 1){
        return "STR";
    } else if (num == 2) {
        return "NUM";
    } else if (num == 3) {
        return "DBL";
    } else {
        printf("%s:%d:%d: error: wrong token number: %d\n"
                ,in_file_name, line_count, char_count_in_line, num);
        exit(-2);
    }
}

// helper fuction for the lexer, maybe should be put in a separate file
int num_matches(char *search) {
    int matches = 0;
    int i = 0;
    while (tokens[i][0] != 0) {
        if (strstr(tokens[i], search) == tokens[i]) {
            matches++;
        }
        i++;
    }
    return matches;
}

bool isAlphaNumeric(char toCheck) {
    if ((toCheck >= 'a' && toCheck <= 'z')
        || (toCheck >= 'A' && toCheck <= 'Z')
        || (toCheck >= '0' && toCheck <= '9')
        || (toCheck == '.' || toCheck == '_')) {
        return true;
    } else {
        return false;
    }
}

bool isDigit(char toCheck) {
    if (toCheck >= '0' && toCheck <= '9') {
        return true;
    } else {
        return false;
    }
}

bool isLetter(char toCheck) {
    if ((toCheck >= 'a' && toCheck <= 'z')
        || (toCheck >= 'A' && toCheck <= 'Z')) {
        return true;
    } else {
        return false;
    }

}

bool isPeriod(char toCheck) {
    if (toCheck == '.') {
        return true;
    } else {
        return false;
    }
}

uint8_t check_string(char *destination, char *toCheck) {
    bool dbl = false;
    int i = 0;
    if (isLetter(toCheck[0])) {
        strcpy(destination, toCheck);
        while (strings[i][0] != 0) {
            if (!strcmp(strings[i], toCheck)) {
                return i + 64;
            }
            i++;
        }
        return 1;
    }
    for (int i = 0; i < (int)strlen(toCheck); i++) {        // need to take another look at that
        if (isPeriod(toCheck[i])) {
            dbl = true;
        } else if (isLetter(toCheck[i])) {
            printf("%s:%d:%d: error: in token: \'%s\', variables can't start with a digit'\n"
                    , in_file_name, line_count, char_count_in_line, queue);
            exit(-2);
        }
    }
    if (dbl) {
        strcpy(destination, toCheck);
        return 3;
    } else {
        strcpy(destination, toCheck);
        return 2;
    }
}

uint8_t check_token(char *toCheck) {
    int i = 0;
    while (tokens[i][0] != 0) {
        if (!strcmp(toCheck, tokens[i])) {
            if (i == 9) {
                line_count++;
                char_count_in_line = 0;
#           ifdef PLINECOUNT
                printf("line count: %d\n", line_count);
#           endif
            }
            return i + 128;
        }
        i++;
    }
    printf("%s:%d:%d: error: token not found: \'%s\'"
            , in_file_name, line_count, char_count_in_line, toCheck);
    exit(-2);
}

// the main work is done here, this fuction is huge and should be split or reduced
uint8_t lex() {
    char next;
    char temp;
    int matches;
    int token_num;
    while ((next = fgetc(in_file)) != EOF) {
        queue_append(next);
        char_count++;
        char_count_in_line++;
#   ifdef PCHARCOUNT
        printf("char count: %d\n", char_count);
#   endif
#   ifdef PCHARCOUNTIL
        printf("char count in line: %d\n", char_count_in_line);
#   endif
#   ifdef PCHAR
        printf("char: \'%c\'\n", next);
#   endif
        if (isAlphaNumeric(next)) {
            if (queue_len() > 1 && !isAlphaNumeric(queue[strlen(queue) - 2])) {
                temp = queue[strlen(queue) - 1];
                queue[strlen(queue) - 1] = 0;
                matches = num_matches(queue);
#           ifdef PMATCHES
                printf("%d\n", matches);
#           endif
                if (matches > 0) {
#               ifdef PQUEUE
                    printf("queue: \'%s\'\n", queue);
#               endif
                    token_num = check_token(queue);
                    empty_queue();
#               ifdef PQUEUE
                    printf("queue: \'%s\'\n", queue);
#               endif
#               ifdef PTEMP
                    printf("%c\n", temp);
#               endif
                    queue_append(temp);
#               ifdef PQUEUE
                    printf("queue: \'%s\'\n", queue);
#               endif
                    return token_num;
                } else {
                    printf("%s:%d:%d: error: wrong token: \'%s\'\n"
                            , in_file_name, line_count, char_count_in_line, queue);
                    exit(-2);
                }
            }
            // this is kept here only for use if something is messing up my string
           // else if (strlen(string) > 0) {
            //    strcpy(string, "");
            //}
        } else {
            if (queue_len() > 1 && isAlphaNumeric(queue[strlen(queue) - 2])) {
                temp = queue[strlen(queue) - 1];
                queue[strlen(queue) - 1] = 0;
                token_num = check_string(string, queue);
#           ifdef PSTRING
                printf("string: %s\n", string);
#           endif
                empty_queue();
                queue_append(temp);
                return token_num;
            }
#           ifdef PQUEUE
                printf("queue: \'%s\'\n", queue);
#           endif
            matches = num_matches(queue);
            if (matches == 1) {
#           ifdef PQUEUE
                printf("queue: \'%s\'\n", queue);
#           endif
                token_num = check_token(queue);
                empty_queue();
                return token_num;
            } else if (matches == 0) {
                temp = queue[strlen(queue) - 1];    // huge problem if queue is empty
                queue[strlen(queue) - 1] = 0;
#           ifdef PTEMP
                printf("%c\n", temp);
#           endif
                if (num_matches(queue) > 0) {
#               ifdef PQUEUE
                    printf("queue: \'%s\'\n", queue);
#               endif
                    token_num = check_token(queue);
                    empty_queue();
                    queue_append(temp);
                    return token_num;
                } else {
                    printf("%s:%d:%d: error: wrong token: \'%s\'\n"
                            , in_file_name, line_count, char_count_in_line, queue);
                    exit(-2);
                }
            }
        }
    }
#ifdef PEOF
    printf("End of file\n");
#endif
    return 255;
}

uint8_t lexer() {
    int token_type = lex();
    while (token_type != 255) {
        // add tokens here to be excluded
        if (token_type != 135 && token_type != 136 && token_type != 137) {
#       ifdef PRETTOKEN
            printf("returning: %d\n", token_type);
#       endif
            return token_type;
        }
        token_type = lex();
    }
    return token_type;
}

