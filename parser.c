// Copyright (c) 2019 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include "lexer.h"
#include "stack.h"
uint8_t stack[STACKSIZE];

#include "classes.h"

#include "parser.h"
#include "codes.h"

extern char in_file_name[];

const uint8_t *VAR_rules[] = {
    (uint8_t []) {ORB, VAR, CRB, 0},
    (uint8_t []) {VAR, INCOP, 0},
    (uint8_t []) {STR, 0}
};

const uint8_t *UN_rules[] = {
    (uint8_t []) {ORB, ASSIGN, CRB, 0},
    (uint8_t []) {INCOP, STR, 0},
    (uint8_t []) {UNOP, UN, 0},
    (uint8_t []) {STR, 0},
    (uint8_t []) {NUM, 0}
};

const uint8_t *MUL_rules[] = {
    (uint8_t []) {MUL, MULOP, UN, 0},
    (uint8_t []) {UN, 0},
};

const uint8_t *ADD_rules[] = {
    (uint8_t []) {ADD, ADDOP, MUL, 0},
    (uint8_t []) {MUL, 0},
};

const uint8_t *ASSIGN_rules[] = {
    (uint8_t []) {STR, ASSIGNOP, ASSIGN, 0},
    (uint8_t []) {ADD, 0}
};

const uint8_t *DECL_rules[] = {
    (uint8_t []) {STR, STR, 0}
};

int has_other_char(uint8_t *stack, const uint8_t *rule) {
    int i = 0;
    uint8_t temp;
    while ((temp = rule[i]) != 0) {
        for (int j = 0; j < (int) strlen((char*) stack); j++) {
            if (temp == stack[j]) {
                continue;
            } else if (j == (int) strlen((char*) stack) - 1) {
                return j;
            }
        }
        i++;
    }
    return -1;
}

bool shift() {
    uint8_t token = lexer();
    if (token == 255) {
        return false;
    } else {
        push(token);
        return true;
    }
}

void reduce(uint8_t reduce_to, uint8_t *stack, const uint8_t *rule) {
    //print_stack();
    if (strlen((char*) rule) == 1) {
        printf("single reduction\n");
        stack[0] = reduce_to;
    } else if (strlen((char*) rule) == strlen((char*) stack)) {
        printf("simple reduction\n");
        for (int i = 0; i < (int) strlen((char*) rule); i++) {
            pop();
        }
        push(reduce_to);
    } else {
        printf("complex reduction\n");
        strcpy((char*) (stack + 1), (char*) (stack + strlen((char*) rule)));
        stack[0] = reduce_to;
        memset(stack + strlen((char*) stack), 0, strlen((char*) rule));
    }
    //print_stack();
}

bool isINCOP(uint8_t op) {
    if (op == INC
            || op == DEC
            || op == INCOP) {
        return true;
    } else {
        return false;
    }
}

bool isUNOP(uint8_t op) {
    if (op == NOT
            || op == MNS
            || op == UNOP) {
        return true;
    } else {
        return false;
    }
}

bool isMULOP(uint8_t op) {
    if (op == TMS
            || op == DVD
            || op == MULOP) {
        //printf("%d is a MULOP\n", op);
        return true;
    } else {
        //printf("%d is not a MULOP\n", op);
        return false;
    }
}

bool isADDOP(uint8_t op) {
    if (op == PLS
            || op == MNS
            || op == ADDOP) {
        //printf("%d is an ADDOP\n", op);
        return true;
    } else {
        //printf("%d is not an ADDOP\n", op);
        return false;
    }
}

bool isASSIGNOP(uint8_t op) {
    if (op == EQL
            || op == ASSIGNOP) {
        return true;
    } else {
        return false;
    }
}

bool isType(uint8_t op) {
    if (op == INT
            || op == DOUBLE
            || op == VOID
            || op == TYPE) {
        return true;
    } else {
        return false;
    }
}

Operand* PUN(uint8_t stack[]) {
    int op_type;
    Operand *op1;
    printf("need to solve un: %d\n", stack[0]);
    do {
        if (!shift()) {
            printf("%s:%d:%d: error: expecting more tokens for PMUL\n"
                , in_file_name, get_line_count()
                , get_char_count_in_line());
            exit(-4);
        }
        //printf("solving MUL\n");
        //print_stack();
        if (stack[0] == ORB) {
            op1 = PASSIGN(stack + 1);
        }

        if (stack[0] == ORB && stack[1] == ASSIGN && stack[2] == CRB) {
            reduce(UN, stack, UN_rules[0]);
        } else if (isINCOP(stack[0]) && stack[1] == STR) {
            printf("found a match\n");
            Uni_gate *gate = new_Uni_gate();
            set_Ugate_type(gate, stack[0]);
            op1 = new_Operand(get_string(), CHR);
            set_Ugate_operand1(gate, op1);
            op1 = new_Operand(gate, VDU);

            reduce(UN, stack, UN_rules[1]);
        } else if (isUNOP(stack[0]) && stack[1] == UN) {
            Uni_gate *gate = new_Uni_gate();
            set_Ugate_type(gate, stack[0]);
            set_Ugate_operand1(gate, op1);
            op1 = new_Operand(gate, VDU);

            reduce(UN, stack, UN_rules[2]);
        } else if (stack[0] == STR
                || stack[0] == NUM) {
            op1 = new_Operand(get_string(), CHR);

            stack[0] = UN;
        }
    } while (strstr((char*) UN_rules[0], (char*) stack)
            || strstr((char*) UN_rules[1], (char*) stack)
            || strstr((char*) UN_rules[2], (char*) stack)
            || strstr((char*) UN_rules[4], (char*) stack)
            );
    print_stack();
    return op1;
}

Operand* PMUL(uint8_t stack[]) {
    int op_type;
    Operand *op1;
    Operand *op2;
    do {
        if (strstr((char*) MUL_rules[0], (char*) stack)) {
            if (!shift()) {
                printf("%s:%d:%d: error: expecting more tokens for PMUL\n"
                    , in_file_name, get_line_count()
                    , get_char_count_in_line());
                exit(-4);
            }
        }
        //printf("solving MUL\n");
        //print_stack();
        if (has_other_char(stack, ADD_rules[0]) != -1) {
            op2 = PUN(has_other_char(stack, MUL_rules[0]) + stack);
        }
        if (strstr((char*) stack, (char*) MUL_rules[0])) {
            //printf("operation: %d:\t%d, %d\n", op_type, op1, get_string());
            //strcpy(op1, "gate");
            Bin_gate *gate = new_Bin_gate();
            set_Bgate_type(gate, op_type);
            set_Bgate_operand1(gate, op1);
            set_Bgate_operand2(gate, op2);
            op1 = new_Operand(gate, VDB);
            reduce(MUL, stack, MUL_rules[0]); } else if (strstr((char*) stack, (char*) MUL_rules[1])) { //strcpy(op1, get_string());
            op1 = op2;
            reduce(MUL, stack, MUL_rules[1]);
        }
        if (isMULOP(stack[strlen((char*) stack) - 1])) {
            op_type = stack[strlen((char*) stack) - 1];
            stack[strlen((char*) stack) - 1] = MULOP;
        }
    } while (strstr((char*) MUL_rules[0], (char*) stack));
    return op1;
}

Operand* PADD(uint8_t stack[]) {
    int op_type;
    //char op2[20];
    Operand *op1;
    Operand *op2;
    printf("solve ADD with stack: %d\n", stack[0]);
    do {
        if (strstr((char*) ADD_rules[0], (char*) stack)) {
            if (!shift()) {
                printf("%s:%d:%d: error: expecting more tokens for PADD\n"
                    , in_file_name, get_line_count()
                    , get_char_count_in_line());
                exit(-4);
            }
        }
        //printf("solving ADD\n");
        //print_stack();
        if (has_other_char(stack, ADD_rules[0]) != -1) {
            op2 = PMUL(has_other_char(stack, ADD_rules[0]) + stack);
        }
        if (stack[0] == ADD && isADDOP(stack[1]) && stack[2] == MUL) {
            //printf("operation: %d:\t%d, %d\n", op_type, op2, op1);
            //strcpy(op2, "gate");
            Bin_gate *gate = new_Bin_gate();
            set_Bgate_type(gate, op_type);
            set_Bgate_operand1(gate, op1);
            set_Bgate_operand2(gate, op2);
            op1 = new_Operand(gate, VDB);
            reduce(ADD, stack, ADD_rules[0]);
        } else if (stack[0] == MUL) {
            //strcpy(op2, op1);
            op1 = op2;
            reduce(ADD, stack, ADD_rules[1]);
        }
        if (isADDOP(stack[strlen((char*) stack) - 1])) {
            op_type = stack[strlen((char*) stack) - 1];
            stack[strlen((char*) stack) - 1] = ADDOP;
        }
    } while (strstr((char*) ADD_rules[0], (char*) stack));
    return op1;
}

Operand* PASSIGN(uint8_t stack[]) {
    int op_type;
    Operand *op1;
    Operand *op2;
    printf("solve ASSIGN with stack: %d\n", stack[0]);
    do {
        if (strstr((char*) ASSIGN_rules[0], (char*) stack)) {
            if (!shift()) {
                printf("%s:%d:%d: error: expecting more tokens for PASSIGN\n"
                    , in_file_name, get_line_count()
                    , get_char_count_in_line());
                exit(-4);
            }
        }
        printf("assign here\n");
        print_stack();
        //printf("%d %d\n", stack[0] == STR, isASSIGNOP(stack[1]));
        if (stack[0] == STR && isASSIGNOP(stack[1])) {
            op_type = stack[1];
            op1 = new_Operand(get_string(), CHR);
            stack[1] = ASSIGNOP;
        }
        printf("size: %d\n", strlen(stack));
        if (strlen(stack) >= 2 && !isASSIGNOP(stack[1]) || stack[0] != STR) {
            op1 = PADD(stack);
            reduce(ASSIGN, stack, ASSIGN_rules[1]);
        }
        if (stack[0] == STR && stack[1] == ASSIGNOP && stack[2] != ASSIGN) {
            op2 = PASSIGN(stack + 2);
        }
        if (stack[0] == STR && stack[1] == ASSIGNOP && stack[2] == ASSIGN) {
            reduce(ASSIGN, stack, ASSIGN_rules[0]);
            Bin_gate *gate = new_Bin_gate();
            set_Bgate_type(gate, op_type);
            set_Bgate_operand1(gate, op1);
            set_Bgate_operand2(gate, op2);
            op1 = new_Operand(gate, VDB);
        }
    } while (strstr((char*) ASSIGN_rules[0], (char*) stack));
    return op1;
}

Operand *PDECL(uint8_t stack[]) {
    Operand *op1;
    if (strlen(stack) == 0) {
        shift();
        shift();
        shift();
    } else if (strlen(stack) == 1) {
        shift();
        shift();
    } else if (strlen(stack) == 2){
        shift();
    }
    if (isType(stack[0]) && stack[1] == STR) {
        Uni_gate *gate = new_Uni_gate();
        set_Ugate_type(gate, stack[0]);
        op1 = new_Operand(get_string(), CHR);
        set_Ugate_operand1(gate, op1);
        op1 = new_Operand(gate, VDU);
        printf("A declaration of %s has %s\n", get_token(stack[0]), gate->operand1->op);
        printf("Stack -----------------------------\n");
        print_stack();
        reduce(DECL, stack, DECL_rules[0]);
        printf("Stack -----------------------------\n");
        print_stack();
    } else {
        printf("wrong input\n");
    }
    return op1;
}

Inst *PINST(uint8_t stack[]) {
    Inst *inst;
    if (!shift()) {
        printf("Done Parsing\n");
        return NULL;
    }
    printf("type of operend: %d\n", isType(stack[0]));
    if (isType(stack[0])) {
        inst = new_Inst(PDECL(stack));
    } else {
        inst = new_Inst(PASSIGN(stack));
    }
    if (stack[1] != SMCLN) {
        printf("wrong instruction\n");
    } else {
        stack[1] = 0;
    }
    empty_stack();
    return inst;
}

Inst *parser() {
    // remember to call the lowest precedence function
    Inst *root = PINST(stack);
    Inst *next, *last = root;
    while ((next = PINST(stack)) != NULL) {
        set_inst_next(last, next);
        last = next;
    }

    printf("AST: %ld\n", (long) last);
    print_stack();
    if (stack[1] == SMCLN) {
        stack[1] = 0;
    }
    if (strlen((char*) stack) > 1) {
        if (stack[1] == STR || stack[1] == NUM || stack[1] == DBL) {
            printf("%s:%d:%d: error: wrong input: \'%s\'\n"
                , in_file_name, get_line_count()
                , get_char_count_in_line(), get_string());
        } else {
            printf("%s:%d:%d: error: wrong input: \'%s\'\n"
                    , in_file_name, get_line_count()
                    , get_char_count_in_line(), get_token(stack[2]));
        }
    }
    printf("token: %s\n", get_string());
    print_stack();

    return root;
}

